package models

import (
	"github.com/jinzhu/gorm"
)

type Comment struct {
	gorm.Model
	// CommentDate time.Time `gorm:"column:comment_date"`
	Topic       string `gorm:"column:topic"`
	Description string `gorm:"column:description"`
	UserID      uint   `gorm:"column:user_id"`
	TipID       uint   `gorm:"column:tipID"`
}
type CommentService interface {
	CreateComment(comment *Comment) error
	GetComment(id uint) ([]Comment, error)
}
type commentGorm struct {
	db *gorm.DB
}

func NewCommentService(db *gorm.DB) CommentService {
	return &commentGorm{db}
}
func (cg *commentGorm) CreateComment(comment *Comment) error {
	return cg.db.Create(comment).Error
}

func (cg *commentGorm) GetComment(id uint) ([]Comment, error) {
	comment := []Comment{}
	if err := cg.db.Where("tipID = ?", id).Find(&comment).Error; err != nil {
		return nil, err
	}
	return comment, nil

}
