package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type ListPet struct {
	gorm.Model
	Name           string `gorm:"column:name`
	Gene           string `gorm:"column:gene`
	Type           string `gorm:"column:type`
	Sex            string `gorm:"column:sex"`
	Age            uint   `gorm:"column:age"`
	ContractNumber string `gorm:"column:contractNumber"`
	Desease        string `gorm:"column:desease"`
	Description    string `gorm:"column:description"`
	Address        string `gorm:"column:address"`
	Status         bool   `gorm:"column:status"`
	Latitude       string `gorm:"column:latitude"`
	Longitude      string `gorm:"column:longitude"`
	Image          string
	ShelterID      uint `gorm:"not null"`
	UserID         uint `gorm:"not null"`
	// User           User   `gorm:"column:user"`
}
type ListPetService interface {
	CreateListPet(listPet *ListPet) error
	ShowAllListPet() ([]ListPet, error)
	ShowHistoryAddPet(id uint) ([]ListPet, error)
	ListAllAccept() ([]ListPet, error)
	ListAllNotAccept() ([]ListPet, error)
	ListOne(id uint) (*ListPet, error)
	DeleteAdd(id uint) error
	UpdateStatusPet(id uint, status bool) error
	UpdateShelter(id uint, ShelterID uint) error
	ShowHistoryAcceptAdd(id uint) ([]ListPet, error)
	// UpdateAllList(listPet *ListPet)
	UpdatePetImage(id uint, image string) error
}

type ListPetGorm struct {
	db *gorm.DB
}

func NewListPetService(db *gorm.DB) ListPetService {
	return &ListPetGorm{db}
}

//--------------image--------------------
func (lpg *ListPetGorm) UpdatePetImage(id uint, image string) error {
	return lpg.db.Model(&ListPet{}).Where("id = ?", id).
		Update("image", image).Error
}

//-----------------List PET-----------------------
func (lpg *ListPetGorm) CreateListPet(listPet *ListPet) error {
	return lpg.db.Create(listPet).Error
}

func (lpg *ListPetGorm) ShowAllListPet() ([]ListPet, error) {
	newListPet := []ListPet{}
	if err := lpg.db.Order("updated_at desc").Find(&newListPet).Error; err != nil {
		return nil, err
	}
	return newListPet, nil
}

func (lpg *ListPetGorm) ShowHistoryAddPet(id uint) ([]ListPet, error) {
	newListPet := []ListPet{}
	if err := lpg.db.Where("user_id = ?", id).Find(&newListPet).Error; err != nil {
		return nil, err
	}
	return newListPet, nil
}
func (lpg *ListPetGorm) ShowHistoryAcceptAdd(id uint) ([]ListPet, error) {
	newListPet := []ListPet{}
	if err := lpg.db.Where("shelter_id = ?", id).Find(&newListPet).Error; err != nil {
		return nil, err
	}
	return newListPet, nil
}

func (lpg *ListPetGorm) ListAllAccept() ([]ListPet, error) {
	newListPet := []ListPet{}
	err := lpg.db.
		Where("status = ?", true).
		Find(&newListPet).Error
	if err != nil {
		return nil, err
	}
	return newListPet, nil
}

func (lpg *ListPetGorm) ListAllNotAccept() ([]ListPet, error) {
	newListPet := []ListPet{}
	err := lpg.db.
		Where("status = ?", false).
		Find(&newListPet).Error
	if err != nil {
		return nil, err
	}
	return newListPet, nil
}

func (lpg *ListPetGorm) ListOne(id uint) (*ListPet, error) {
	newOnePet := new(ListPet)
	if err := lpg.db.First(newOnePet, id).Error; err != nil {
		return nil, err
	}
	fmt.Println("id====>", id)
	fmt.Println("newOnePet====>", newOnePet)
	return newOnePet, nil
}

func (lpg *ListPetGorm) DeleteAdd(id uint) error {
	return lpg.db.Where("id = ?", id).Delete(&ListPet{}).Error
}

func (lpg *ListPetGorm) UpdateAllList(listPet *ListPet) error {
	if err := lpg.db.Model(listPet).Where("id = ?", listPet.ID).Update(listPet).Error; err != nil {
		return err
	}
	return nil
}

func (lpg *ListPetGorm) UpdateStatusPet(id uint, status bool) error {
	return lpg.db.Model(&ListPet{}).Where("id = ?", id).Update("status", status).Error
}
func (lpg *ListPetGorm) UpdateShelter(id uint, ShelterID uint) error {
	return lpg.db.Model(&ListPet{}).Where("id = ?", id).Update("ShelterID", ShelterID).Error
}
