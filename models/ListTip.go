package models

import (
	"github.com/jinzhu/gorm"
)

type ListTip struct {
	gorm.Model
	Topic       string `gorm:"column:topic`
	Description string `gorm:"column:description`
	UserID      uint   `gorm:"not null"`
}
type ListTipService interface {
	CreateListTip(listTip *ListTip) error
	ShowAllListTip() ([]ListTip, error)
	ShowHistoryAddTip(id uint) ([]ListTip, error)
	DeleteTip(id uint) error
}

type ListTipGorm struct {
	db *gorm.DB
}

func NewListTipService(db *gorm.DB) ListTipService {
	return &ListTipGorm{db}
}

func (ltg *ListTipGorm) CreateListTip(listTip *ListTip) error {
	return ltg.db.Create(listTip).Error
}
func (ltg *ListTipGorm) ShowAllListTip() ([]ListTip, error) {
	newListTip := []ListTip{}
	if err := ltg.db.Find(&newListTip).Error; err != nil {
		return nil, err
	}
	return newListTip, nil
}
func (ltg *ListTipGorm) ShowHistoryAddTip(id uint) ([]ListTip, error) {
	newListTip := []ListTip{}
	if err := ltg.db.Where("user_id = ?", id).Find(&newListTip).Error; err != nil {
		return nil, err
	}
	return newListTip, nil
}
func (ltg *ListTipGorm) DeleteTip(id uint) error {
	return ltg.db.Where("id = ?", id).Delete(&ListTip{}).Error
}
