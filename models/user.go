package models

import (
	"fmt"
	"mi-you/hash"
	"mi-you/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Username  string `gorm:"unique_index;not null"`
	Password  string `gorm:"not null"`
	Email     string `gorm:"not null"`
	Firstname string `gorm:"not null"`
	Lastname  string `gorm:"not null"`
	Phone     string `gorm:"not null"`
	Address   string `gorm:"not null"`
	Token     string `gorm:"unique_index"`
}
type userGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

func NewUserService(db *gorm.DB, key string) UserService {
	hmac := hash.NewHMAC(key)
	return &userGorm{db, hmac}

}

type UserService interface {
	GetByToken(token string) (*User, error)

	SignupUser(user *User) error
	LoginUser(user *User) (string, error)
	LogoutUser(user *User) error
}

func (ug *userGorm) GetByToken(token string) (*User, error) {
	tokenHash := ug.hmac.Hash(token)
	var user User
	err := ug.db.Where("token = ?", tokenHash).First(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, err
}

func (ug *userGorm) SignupUser(tmp *User) error {
	user := new(User)
	user.Username = tmp.Username
	user.Phone = tmp.Phone
	user.Email = tmp.Email
	user.Address = tmp.Address
	user.Lastname = tmp.Lastname
	user.Firstname = tmp.Firstname
	user.Password = tmp.Password

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 12)

	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	tokenHash := ug.hmac.Hash(token)

	user.Token = tokenHash
	tmp.Token = token
	return ug.db.Create(user).Error

}

func (ug *userGorm) LoginUser(user *User) (string, error) {
	fmt.Println("=======================================called")
	dbUser := new(User)
	err := ug.db.Where("Username = ?", user.Username).First(&dbUser).Error
	if err != nil {
		return "error not match email", err
	}
	fmt.Println("from req got===>", user.Password)
	fmt.Println("from db got===>", dbUser.Password)
	bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(dbUser.Password))
	if err != nil {
		fmt.Println("ERROR", err)
		return "error by compare password==>", err
	}

	token, err := rand.GetToken()
	if err != nil {
		return "err random token =>>", err
	}
	// fmt.Println("1--->", token)
	// fmt.Println("2--->", dbUser.Token)
	tokenHash := ug.hmac.Hash(token)

	err = ug.db.Model(&User{}).Where("id = ?", dbUser.ID).Update("token", tokenHash).Error
	if err != nil {
		return "error by not update", err
	}
	user.Token = token
	return token, nil
}

func (ug *userGorm) LogoutUser(user *User) error {
	return ug.db.Model(user).Where("user_id=?", user.ID).Update("token", "").Error
}
func (ug *userGorm) GetByID(id uint) (*User, error) {
	user := new(User)
	if err := ug.db.First(user, id).Error; err != nil {
		return nil, err
	}
	return user, nil
}
