package main

import (
	"log"
	"mi-you/config"
	"mi-you/handlers"
	"mi-you/middleware"
	"mi-you/models"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	if err := db.AutoMigrate(
		&models.User{},
		&models.ListPet{},
		&models.Comment{},
		&models.ListTip{},
	).Error; err != nil {
		log.Fatal(err)
	}
	if conf.Mode == "dev" {
		db.LogMode(true)
	}
	var hmac = "galgall"

	us := models.NewUserService(db, hmac)
	lts := models.NewListTipService(db)
	lps := models.NewListPetService(db)

	cs := models.NewCommentService(db)

	uh := handlers.NewUserHandler(us)
	lth := handlers.NewListTipHandler(lts)
	lph := handlers.NewListPetHandler(lps)

	ch := handlers.NewCommentHandler(cs)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	// r.Static("/upload", "./upload")

	// r.GET("/listOneAddPet", lph.ListOneAddPet)
	r.POST("/login", uh.Login)
	r.POST("/signup", uh.SignupUser)
	// r.GET("/comment/:id", ch.GetComment)

	// r.GET("/alllistpet", lph.ShowAllListPet)
	r.GET("/listpetAll", lph.ShowAllListPet)

	auth := r.Group("/")
	auth.Use(middleware.RequireUser(us))
	{
		auth.POST("/logout", uh.Logout)
		auth.GET("/profile", uh.ShowProfile)

		auth.POST("/listtip", lth.PostListTip)
		auth.POST("/listpet", lph.PostListPet)

		auth.GET("/historylisttip", lth.ShowHistoryAddTip)
		auth.GET("/historylistpet", lph.ShowHistoryAddPet)

		auth.GET("/alllisttip", lth.ShowAllListTip)
		auth.GET("/alllistpet", lph.ShowAllListPet)

		auth.GET("/historyAccept", lph.ShowHistoryAcceptAdd)
		auth.GET("/acceptlist", lph.ListAllAccept)
		auth.GET("/notacceptlist", lph.ListAllNotAccept)

		auth.GET("/listpet/:id", lph.ListOne)
		auth.POST("/comment/:id", ch.AddComment)
		auth.GET("/comment/:id", ch.GetComment)

		auth.DELETE("/listpet/:id", lph.DeleteAdd)
		auth.DELETE("/listTip/:id", lth.DeleteTip)

		auth.PUT("/listpet/:id/Accept", lph.UpdateStatusPet)
		auth.PUT("/listpet/:id/AcceptShelter", lph.UpdateShelter)

		auth.PUT("/listpet", lph.UpdatePetImage)

	}
	r.Run()

}
