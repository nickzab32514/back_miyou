package handlers

import (
	"fmt"
	"mi-you/context"
	"mi-you/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ListPetHandler struct {
	lps models.ListPetService
}
type ListPet struct {
	ID             uint   `json:"id"`
	Name           string `json:"name"`
	Type           string `json:"type"`
	Gene           string `json:"gene"`
	Sex            string `json:"sex"`
	Age            uint   `json:"age"`
	ContractNumber string `json:"contractNumber"`
	Desease        string `json:"desease"`
	Description    string `json:"description"`
	Address        string `json:"address"`
	Latitude       string `json:"latitude"`
	Longitude      string `่json:"longitude"`
	Status         bool   `json:"status"`
	Image          string `json:"image"`
	ShelterID      uint   `json:"shelterID"`
}

type ListPetRes struct {
	ListPet
}
type UpdateStatusReq struct {
	Status bool `json:"status"`
}
type UpdateShelterReq struct {
	ShelterID uint `json:"shelterID"`
}
type UpdateImageReq struct {
	Image     string `json:"image"`
	ListPetID uint   `json:"id"`
}

func NewListPetHandler(lps models.ListPetService) *ListPetHandler {
	return &ListPetHandler{lps}
}

func (lph *ListPetHandler) PostListPet(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	req := new(ListPet)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	listpet := new(models.ListPet)
	listpet.Name = req.Name
	listpet.Gene = req.Gene
	listpet.Type = req.Type
	listpet.Sex = req.Sex
	listpet.Age = req.Age
	listpet.ContractNumber = req.ContractNumber
	listpet.Desease = req.Desease
	listpet.Description = req.Description
	listpet.Address = req.Address
	listpet.Latitude = req.Latitude
	listpet.Longitude = req.Longitude
	listpet.Gene = req.Gene
	listpet.Image = req.Image
	listpet.UserID = user.ID
	err := lph.lps.CreateListPet(listpet)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(ListPetRes)
	res.ID = listpet.ID
	res.Name = listpet.Name
	res.Gene = listpet.Gene
	res.Sex = listpet.Sex
	res.Age = listpet.Age
	res.Type = listpet.Type
	res.Latitude = listpet.Latitude
	res.Longitude = listpet.Longitude
	res.ContractNumber = listpet.ContractNumber
	res.Desease = listpet.Desease
	res.Description = listpet.Description
	res.Address = listpet.Address
	res.Image = listpet.Image
	res.Status = listpet.Status
	c.JSON(201, res)
}
func (lph *ListPetHandler) ShowAllListPet(c *gin.Context) {
	data, err := lph.lps.ShowAllListPet()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
func (lph *ListPetHandler) ShowHistoryAddPet(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := lph.lps.ShowHistoryAddPet(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
func (lph *ListPetHandler) ShowHistoryAcceptAdd(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := lph.lps.ShowHistoryAcceptAdd(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}

func (lph *ListPetHandler) ListAllAccept(c *gin.Context) {
	data, err := lph.lps.ListAllAccept()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
func (lph *ListPetHandler) ListAllNotAccept(c *gin.Context) {
	data, err := lph.lps.ListAllNotAccept()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
func (lph *ListPetHandler) ListOne(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	dataPet, err := lph.lps.ListOne(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, dataPet)
}
func (lph *ListPetHandler) DeleteAdd(c *gin.Context) {
	// user := context.User(c)
	// if user == nil {
	// 	c.Status(401)
	// 	return
	// }
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
	}
	err = lph.lps.DeleteAdd(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err,
		})
	}
	c.JSON(204, gin.H{
		"message": "delete successful",
	})
}

func (lph *ListPetHandler) UpdateStatusPet(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, err)
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, err)
		return
	}
	fmt.Printf("req ==> %v", req)

	err = lph.lps.UpdateStatusPet(uint(id), req.Status)
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.Status(204)
}
func (lph *ListPetHandler) UpdateShelter(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, err)
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, err)
		return
	}
	fmt.Printf("req ==> %v", req)

	err = lph.lps.UpdateShelter(uint(id), user.ID)
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.Status(204)
}

//---------------image-------------------

// func (lph *ListPetHandler) UpdatePetImage(c *gin.Context) {
// 	user := context.User(c)
// 	if user == nil {
// 		c.Status(401)
// 		return
// 	}
// 	req := new(UpdateImageReq)
// 	if err := c.BindJSON(req); err != nil {
// 		c.JSON(400, err)
// 	}
// 	err = lph.lps.UpdatePetImage(user.ID, req.Image)
// 	if user == nil {
// 		c.Status(500)
// 		return
// 	}
// 	c.JSON(200, gin.H{
// 		"id":    user.ID,
// 		"image": req.Image,
// 	})

// }
func (lph *ListPetHandler) UpdatePetImage(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	req := new(UpdateImageReq)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	fmt.Println("req====>", req)
	fmt.Println(req)
	listpet := new(models.ListPet)
	listpet.Image = req.Image
	err := lph.lps.UpdatePetImage(user.ID, listpet.Image)
	if err != nil {
		c.Status(500)
		return
	}
	listpetes := &ListPet{
		ID:    req.ListPetID,
		Image: listpet.Image,
	}
	c.JSON(200, listpetes)
}
