package handlers

import (
	"mi-you/context"
	"mi-you/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ListTipHandler struct {
	lts models.ListTipService
}

func NewListTipHandler(lts models.ListTipService) *ListTipHandler {
	return &ListTipHandler{lts}
}

type ListTip struct {
	ID          uint   `json:"id"`
	Topic       string `json:"topic"`
	Description string `json:"description"`
}
type ListTipRes struct {
	ListTip
}

func (lth *ListTipHandler) PostListTip(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	req := new(ListTip)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	tip := new(models.ListTip)
	tip.Topic = req.Topic
	tip.Description = req.Description
	tip.UserID = user.ID
	err := lth.lts.CreateListTip(tip)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(ListTipRes)
	res.ID = tip.ID
	res.Topic = tip.Topic
	res.Description = tip.Description
	c.JSON(201, res)
}
func (lth *ListTipHandler) ShowAllListTip(c *gin.Context) {
	data, err := lth.lts.ShowAllListTip()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
func (lth *ListTipHandler) ShowHistoryAddTip(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := lth.lts.ShowHistoryAddTip(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}

func (lth *ListTipHandler) DeleteTip(c *gin.Context) {

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
	}
	err = lth.lts.DeleteTip(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err,
		})
	}
	c.JSON(204, gin.H{
		"message": "delete successful",
	})
}
