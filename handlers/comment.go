package handlers

import (
	"fmt"
	"mi-you/context"
	"mi-you/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Comment struct {
	Topic       string `json:"topic"`
	Description string `json:"description"`
	UserID      uint   `json:"user_id"`
}
type CommnetHandler struct {
	cs models.CommentService
}

func NewCommentHandler(cs models.CommentService) *CommnetHandler {
	return &CommnetHandler{cs}
}

type AddCommentReq struct {
	Description string
}
type CommentRes struct {
	ID          uint    `json:"id"`
	Topic       string  `json:"topic"`
	Description string  `json:"description"`
	User        UserRes `json:"user"`
}
type UserRes struct {
	ID        uint   `json:"id"`
	Username  string `json:"username"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Image     string `json:"image"`
}

func (ch *CommnetHandler) AddComment(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		fmt.Println(user)
		return
	}
	tipIDStr := c.Param("id")
	id, err := strconv.Atoi(tipIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
		return
	}
	req := Comment{}
	if err := c.BindJSON(&req); err != nil {
		// c.JSON(400, gin.H{
		// 	"message": err.Error(),
		// })
		c.Status(400)
		return
	}
	comment := &models.Comment{}
	comment.Description = req.Description
	comment.Topic = req.Topic
	comment.UserID = user.ID
	comment.TipID = uint(id)

	err = ch.cs.CreateComment(comment)

	if err != nil {
		c.Status(500)
		return
	}
	c.Status(200)
}

func (ch *CommnetHandler) GetComment(c *gin.Context) {
	tipIDStr := c.Param("id")
	id, err := strconv.Atoi(tipIDStr)
	if err != nil {
		c.Status(400)
		return
	}
	comment, err := ch.cs.GetComment(uint(id))
	if err != nil {
		c.Status(500)
		return
	}
	c.JSON(200, comment)
}
