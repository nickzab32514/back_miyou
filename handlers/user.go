package handlers

import (
	"fmt"
	"mi-you/context"
	"mi-you/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Email    string
	Username string
	Password string
	Token    string
}
type ReqUser struct {
	Email     string `่json:"email"`
	Username  string `่json:"username"`
	Password  string `่json:"password"`
	Firstname string `่json:"Firstname"`
	Lastname  string `่json:"Lastname"`
	Phone     string `่json:"Phone"`
	Address   string `่json:"Address"`
}
type LoginReq struct {
	Username string `่json:"username"`
	Password string `่json:"password"`
}

type UserHandler struct {
	us models.UserService
}

func NewUserHandler(us models.UserService) *UserHandler {
	return &UserHandler{us}
}
func (uh *UserHandler) SignupUser(c *gin.Context) {
	req := new(ReqUser)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	var users models.User
	users.Email = req.Email
	users.Address = req.Address
	users.Firstname = req.Firstname
	users.Lastname = req.Lastname
	users.Phone = req.Phone
	users.Username = req.Username

	if err := uh.us.SignupUser(&users); err != nil {
		c.JSON(500, gin.H{
			"message": err,
		})
		return
	}
	c.JSON(201, gin.H{
		"token": users.Token,
	})
}
func (uh *UserHandler) Login(c *gin.Context) {
	req := new(LoginReq)
	fmt.Println(req)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	user := new(models.User)
	user.Username = req.Username
	user.Password = req.Password

	token, err := uh.us.LoginUser(user)

	if err != nil {
		c.JSON(401, gin.H{
			"message": err,
		})
		return
	}
	c.JSON(201, gin.H{"token": token})
}

func (uh *UserHandler) Logout(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	err := uh.us.LogoutUser(user)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(204)
}
func (uh *UserHandler) ShowProfile(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	c.JSON(200, gin.H{
		"id":       user.ID,
		"username": user.Username,
		"fistName": user.Firstname,
		"lastName": user.Lastname,
	})
}
